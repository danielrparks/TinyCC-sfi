// sfi runtime for tcc
#ifdef TCC_TARGET_RISCV64
// jalr x0, 0(x31)
// | offset    ||rs1 ||f||rd  |op    |
// 00000000 00001111 10000000 01100111
__asm__(
  "	.text\n"
  "	.align 4\n"
  "	.global __trampoline\n"
  "	.type __trampoline, %function\n"
  "__trampoline:\n"
  "	.ascii \"g\\x80\\x0f\\x00\"\n"
  "	.size __trampoline, .-__trampoline\n"
);
#endif
