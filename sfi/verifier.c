#include "sfi.h"

int main(int argc, char *argv[])
{
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <module.sfi>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  module_load(argv[1]);
  module_check();
  fprintf(stderr, "PASS\n");
  return 0;
}
