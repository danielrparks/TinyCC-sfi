#ifndef _SFI_H
#define _SFI_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define AND_MASK   0x0fffff
#define TEXT_START 0x200000  /* also text OR_MASK */
#define TEXT_STOP  0x300000
#define DATA_START 0x300000  /* also data OR_MASK */
#define DATA_STOP  0x400000
#define GUARD_SIZE 0x002000  /* 2^13 */

/* yes i know you could do this with preprocessor hacks */
#define AND_MASK_STR   "0x0fffff"
#define TEXT_START_STR "0x200000"
#define TEXT_STOP_STR  "0x300000"
#define DATA_START_STR "0x300000"
#define DATA_STOP_STR  "0x400000"

/*
 * SFI setup.  Call these in order.
 */
void module_load(char *filename);
int module_check(void);
void module_activate(void);

/*
 * SFI interface.  First argument to sfi_call should be
 * a value returned by sfi_entrypoint.  Only integer arguments
 * are supported, and only six or fewer.
 */
uint64_t sfi_entrypoint(char *fname);
uint64_t sfi_call(uint64_t entrypoint, ...);

#endif
