#include "sfi.h"

uint64_t
sfi_call(uint64_t entrypoint, ...);

__asm__(
  "  .globl sfi_call\n"
  "  .p2align 2\n"
  "  .type sfi_call, STT_FUNC\n"
  "sfi_call:\n"
  "  .cfi_startproc\n"
  /* move call target to t0 */
  "  mv t0, a0\n"
  /* save caller registers */
  "  la x31, pcb\n"
  "  sd ra,    0(x31)\n"
  "  sd sp,    8(x31)\n"
  "  sd gp,   16(x31)\n"
  "  sd tp,   24(x31)\n"
  "  sd s0,   32(x31)\n"
  "  sd s1,   40(x31)\n"
  "  sd s2,   48(x31)\n"
  "  sd s3,   56(x31)\n"
  "  sd s4,   64(x31)\n"
  "  sd s5,   72(x31)\n"
  "  sd s6,   80(x31)\n"
  "  sd s7,   88(x31)\n"
  "  sd s8,   96(x31)\n"
  "  sd s9,  104(x31)\n"
  "  sd s10, 112(x31)\n"
  "  sd s11, 120(x31)\n"
  /* shift arguments */
  "  mv a0, a1\n"
  "  mv a1, a2\n"
  "  mv a2, a3\n"
  "  mv a3, a4\n"
  "  mv a4, a5\n"
  "  mv a5, a6\n"
  "  mv a6, a7\n"
  "  mv a7, zero\n"
  /* zero unused registers */
  "  mv gp, zero\n"
  "  mv tp, zero\n"
  "  mv t1, zero\n"
  "  mv t2, zero\n"
  "  mv s1, zero\n"
  "  mv s2, zero\n"
  "  mv s3, zero\n"
  "  mv s4, zero\n"
  "  mv s5, zero\n"
  "  mv s6, zero\n"
  "  mv s7, zero\n"
  "  mv s8, zero\n"
  "  mv s9, zero\n"
  /* set up sfi registers */
  "  li sp,  " DATA_STOP_STR "\n"
  "  li fp,  " DATA_STOP_STR "\n"
  "  li ra,  " TEXT_START_STR "\n"
  "  li x26, " TEXT_START_STR "\n"  /* code pointer */
  "  li x27, " DATA_START_STR "\n"  /* data pointer */
  "  li x28, " TEXT_START_STR "\n"  /* code OR_MASK */
  "  li x29, " DATA_START_STR "\n"  /* data OR_MASK */
  "  li x30, " AND_MASK_STR "\n"    /* code/data AND_MASK */
  "  la x31, sfi_return\n"          /* trampoline target */
  "  jr t0\n"
  ".Lsfi_call_end1:\n"
  "  .size sfi_call, .Lsfi_call_end1-sfi_call\n"
  "  .cfi_endproc"
);

__asm__(
  "  .globl sfi_return\n"
  "  .p2align 2\n"
  "  .type sfi_return, STT_FUNC\n"
  "sfi_return:\n"
  "  .cfi_startproc\n"
  /* restore caller registers */
  "  la x31, pcb\n"
  "  ld ra,    0(x31)\n"
  "  ld sp,    8(x31)\n"
  "  ld gp,   16(x31)\n"
  "  ld tp,   24(x31)\n"
  "  ld s0,   32(x31)\n"
  "  ld s1,   40(x31)\n"
  "  ld s2,   48(x31)\n"
  "  ld s3,   56(x31)\n"
  "  ld s4,   64(x31)\n"
  "  ld s5,   72(x31)\n"
  "  ld s6,   80(x31)\n"
  "  ld s7,   88(x31)\n"
  "  ld s8,   96(x31)\n"
  "  ld s9,  104(x31)\n"
  "  ld s10, 112(x31)\n"
  "  ld s11, 120(x31)\n"
  /* zero unused registers */
  "  mv a2, zero\n"
  "  mv a3, zero\n"
  "  mv a4, zero\n"
  "  mv a5, zero\n"
  "  mv a6, zero\n"
  "  mv a7, zero\n"
  "  mv t0, zero\n"
  "  mv t1, zero\n"
  "  mv t2, zero\n"
  "  mv t3, zero\n"
  "  mv t4, zero\n"
  "  mv t5, zero\n"
  "  mv t6, zero\n"
  "  ret\n"
  ".Lsfi_return_end1:\n"
  "  .size sfi_return, .Lsfi_return_end1-sfi_return\n"
  "  .cfi_endproc"
);
