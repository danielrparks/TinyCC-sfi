# tcc-sfi
Adding riscv64 sfi support to tcc

# Compilation & running instructions
On riscv64 FreeBSD:

```sh
mkdir local
cd tcc
./configure --prefix=$(realpath ../local) --debug --cpu=riscv64 --cc=cc
gmake ONE_SOURCE=no -j
gmake install
cd ../sfi/modules
gmake demo-runner
../../local/bin/tcc -sfi demo.c -o demo
../demo-runner demo
```
